function getReport(){

            let grade = "";  //declare a variable for grade
            let  result="";  //declare a variable for result

            //read the marks
            let engMarks = document.getElementById('txtEnglish').value;
            let bangMarks = document.getElementById('txtBangla').value;
            let mathsMarks = document.getElementById('txtMaths').value;
            let scienceMarks = document.getElementById('txtScience').value;


            //calculate the total marks (using double notation technique)
           

            let totalMarks = engMarks - (- bangMarks) - (- mathsMarks) - (- scienceMarks);
            
            
            //get the average marks
             let averageMarks = totalMarks / 4;  
           


           if (averageMarks > 79 && averageMarks <= 100) {
              grade = "A+"; 
              result="5.00";
           }

           if (averageMarks > 69 && averageMarks <= 79) {
              grade = "A"; 
              result="4.00";
           }

           if (averageMarks > 59 && averageMarks <= 69) {
              grade = "A-"; 
              result="3.50";
           }

           if (averageMarks > 49 && averageMarks <= 59) {
              grade = "B"; 
              result="3.00";
           }

           if (averageMarks > 39 && averageMarks <= 49) {
              grade = "C"; 
              result="2.00";
           }

           if (averageMarks >= 33 && averageMarks <= 39) {
              grade = "D"; 
              result="1.00";
           }

           if (averageMarks < 33) {
              grade = "F"; 
              result="Fail";
           }

                    
          



            //display the results   
            document.getElementById('txtStudentName').value = document.getElementById('txtName').value;
            document.getElementById('txtStudentClass').value = document.getElementById('txtClass').value;
            document.getElementById('txtTotalMarks').value = totalMarks;
            document.getElementById('txtAvgMarks').value = averageMarks;
            document.getElementById('txtGrade').value = grade;
            document.getElementById('txtResult').value = result;

} //end of function getReport