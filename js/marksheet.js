function getReport(){

            let grade = "";  //declare a variable for grade
            let  result="";  //declare a variable for result

            //read the marks
            let engMarks = document.getElementById('txtEnglish').value;
            let bangMarks = document.getElementById('txtBangla').value;
            let mathsMarks = document.getElementById('txtMaths').value;
            let scienceMarks = document.getElementById('txtScience').value;


            //calculate the total marks (using double notation technique)
            let totalMarks = engMarks - (- bangMarks) - (- mathsMarks) - (- scienceMarks);
            
            //get the average marks
            let averageMarks = totalMarks / 4;                  
            
            //find the grade and result using the ternary operator inside the switch statement                  
            switch(
            
                
                    //usage of ternary operator inside switch

                    (averageMarks > 79 && averageMarks <= 100) ? 1 : 
                    (averageMarks > 69 && averageMarks <= 79) ? 2 : 
                    (averageMarks > 59 && averageMarks <= 69) ? 3 : 
                    (averageMarks > 49 && averageMarks <= 59) ? 4 : 
                    (averageMarks > 39 && averageMarks <= 49) ? 5 :
                    (averageMarks >= 33 && averageMarks <= 39) ? 6 :
                    (averageMarks < 33) ? 7 : 0
                  )
                    
                    {
                        case 1 :grade = "A+";result="5.00";break;
                        case 2 :grade = "A"; result="4.00";break;
                        case 3 :grade = "A-"; result="3.50";break;
                        case 4 :grade = "B"; result="3.00";break;
                        case 5 :grade = "C"; result="2.00";break;
                        case 6 :grade = "D"; result="1.00";break;
                        case 7 :grade = "F"; result="Fail";break;
                    }
                    
                
            //display the results   
            document.getElementById('txtStudentName').value = document.getElementById('txtName').value;
            document.getElementById('txtStudentClass').value = document.getElementById('txtClass').value;
            document.getElementById('txtTotalMarks').value = totalMarks;
            document.getElementById('txtAvgMarks').value = averageMarks;
            document.getElementById('txtGrade').value = grade;
            document.getElementById('txtResult').value = result;

} //end of function getReport